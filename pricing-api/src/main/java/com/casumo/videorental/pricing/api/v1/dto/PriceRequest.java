package com.casumo.videorental.pricing.api.v1.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.ZonedDateTime;

import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;

public class PriceRequest {
    private final String rule;
    private final ZonedDateTime start; //TODO: validate start cannot be after end
    private final ZonedDateTime end;

    @JsonCreator
    public PriceRequest(
            @JsonProperty(required = true) String rule,
            @JsonProperty(required = true) String start,
            @JsonProperty(required = true) String end) {
        this.rule = rule;
        this.start = ZonedDateTime.from(ISO_DATE_TIME.parse(start));
        this.end = ZonedDateTime.from(ISO_DATE_TIME.parse(end));
    }

    public String getRule() {
        return rule;
    }

    public ZonedDateTime getStart() {
        return start;
    }

    public ZonedDateTime getEnd() {
        return end;
    }
}
