package com.casumo.videorental.pricing.api.v1.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class PriceBulkResponseEntry {
    private final String uuid;
    private final BigDecimal price;

    @JsonCreator
    public PriceBulkResponseEntry(
            @JsonProperty(required = true) String uuid,
            @JsonProperty(required = true) BigDecimal price) {
        this.uuid = uuid;
        this.price = price;
    }

    public String getUuid() {
        return uuid;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
