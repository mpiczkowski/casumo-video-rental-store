package com.casumo.videorental.pricing.api.v1.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PriceBulkRequest {
    private final List<PriceBulkRequestEntry> items;

    @JsonCreator
    public PriceBulkRequest(@JsonProperty(required = true) List<PriceBulkRequestEntry> items) {
        this.items = items;
    }

    public List<PriceBulkRequestEntry> getItems() {
        return items;
    }
}
