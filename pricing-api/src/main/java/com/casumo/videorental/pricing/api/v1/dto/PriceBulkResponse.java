package com.casumo.videorental.pricing.api.v1.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;

public class PriceBulkResponse {
    private final List<PriceBulkResponseEntry> items;
    private final BigDecimal total;

    @JsonCreator
    public PriceBulkResponse(@JsonProperty(required = true) List<PriceBulkResponseEntry> items,
                             @JsonProperty(required = true) BigDecimal total) {
        this.items = items;
        this.total = total;
    }

    public List<PriceBulkResponseEntry> getItems() {
        return items;
    }

    public BigDecimal getTotal() {
        return total;
    }
}
