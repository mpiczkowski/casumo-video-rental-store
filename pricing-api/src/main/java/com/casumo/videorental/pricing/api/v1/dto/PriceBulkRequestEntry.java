package com.casumo.videorental.pricing.api.v1.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.ZonedDateTime;

import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;

public class PriceBulkRequestEntry {
    private final String uuid;
    private final String rule;
    private final ZonedDateTime start;
    private final ZonedDateTime end;

    @JsonCreator
    public PriceBulkRequestEntry(
            @JsonProperty(required = true, value = "uuid") String uuid,
            @JsonProperty(required = true, value = "rule") String rule,
            @JsonProperty(required = true, value = "start") String start,
            @JsonProperty(required = true, value = "end") String end) {
        this.uuid = uuid;
        this.rule = rule;
        this.start = ZonedDateTime.from(ISO_DATE_TIME.parse(start));
        this.end = ZonedDateTime.from(ISO_DATE_TIME.parse(end));
    }

    public String getUuid() {
        return uuid;
    }

    public String getRule() {
        return rule;
    }

    public ZonedDateTime getStart() {
        return start;
    }

    public ZonedDateTime getEnd() {
        return end;
    }
}
