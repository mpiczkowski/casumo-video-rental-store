package com.casumo.videorental.rental.events;

import lombok.Value;

@Value
public class FilmBorrowedEvent {
    private final String filmCategory;
    private final String filmId;
    private final String filmItemId;
    private final String borrowerId;
}
