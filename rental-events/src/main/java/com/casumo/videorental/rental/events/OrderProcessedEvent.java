package com.casumo.videorental.rental.events;

import lombok.Value;

@Value
public class OrderProcessedEvent {
    private final String orderId;
}
