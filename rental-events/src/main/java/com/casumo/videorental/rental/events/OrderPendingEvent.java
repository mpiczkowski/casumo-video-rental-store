package com.casumo.videorental.rental.events;

import lombok.Value;

@Value
public class OrderPendingEvent {
    private final String orderId;
}
