package com.casumo.videorental;

import com.casumo.videorental.pricing.repository.PriceRatesGenerator;
import com.casumo.videorental.rental.repository.MongoDbIndexesGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
@EnableMongoRepositories(basePackages = {"com.casumo"})
@EntityScan(basePackages = {"com.casumo"})
@ComponentScan(basePackages = {"com.casumo"})
public class VideoRentalStoreApplication {
	@Autowired
	private MongoDbIndexesGenerator rentalIndexGenerator;

    @Autowired
    private PriceRatesGenerator priceRatesGenerator;

	@PostConstruct
	void started() {
		rentalIndexGenerator.ensureIndexes();
        priceRatesGenerator.generate();
	}
	public static void main(String[] args) {
		SpringApplication.run(VideoRentalStoreApplication.class, args);
	}
}
