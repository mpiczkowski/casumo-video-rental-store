package com.casumo.videorental.pricing.api.v1;

import com.casumo.videorental.pricing.api.v1.dto.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class PriceControllerTest {
    private static final String PRICE_RULE_NAME = "regular";

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    public void should_calculate_price_for_bulk() {
        // when
        PriceBulkRequestEntry item1 = new PriceBulkRequestEntry("id1", PRICE_RULE_NAME, "2011-12-03T10:15:30+01:00", "2011-12-04T10:15:30+01:00");
        PriceBulkRequestEntry item2 = new PriceBulkRequestEntry("id2", PRICE_RULE_NAME, "2011-12-03T10:15:30+01:00", "2011-12-04T10:15:30+01:00");
        PriceBulkRequest request = new PriceBulkRequest(Arrays.asList(item1, item2));
        ResponseEntity<PriceBulkResponse> response = testRestTemplate.postForEntity("/v1/prices/bulk", request, PriceBulkResponse.class);

        // then
        assertTrue(response.getStatusCode().is2xxSuccessful());
        assertEquals(BigDecimal.valueOf(60), response.getBody().getTotal());
    }

    @Test
    public void should_calculate_price() {
        // when
        PriceRequest request = new PriceRequest(PRICE_RULE_NAME, "2011-12-03T10:15:30+01:00", "2011-12-04T10:15:30+01:00");
        ResponseEntity<PriceResponse> response = testRestTemplate.postForEntity("/v1/prices", request, PriceResponse.class);

        // then
        assertTrue(response.getStatusCode().is2xxSuccessful());
        assertEquals(BigDecimal.valueOf(30), response.getBody().getPrice());
    }

    @Test
    public void should_fail_calculate_price_for_bulk_when_rule_not_found() {
        // when
        String priceRuleName = "nonexisting";
        PriceBulkRequestEntry item1 = new PriceBulkRequestEntry("id1", priceRuleName, "2011-12-03T10:15:30+01:00", "2011-12-04T10:15:30+01:00");
        PriceBulkRequestEntry item2 = new PriceBulkRequestEntry("id2", priceRuleName, "2011-12-03T10:15:30+01:00", "2011-12-04T10:15:30+01:00");
        PriceBulkRequest request = new PriceBulkRequest(Arrays.asList(item1, item2));
        ResponseEntity<String> response = testRestTemplate.postForEntity("/v1/prices/bulk", request, String.class);

        // then
        assertTrue(response.getStatusCode().is5xxServerError());
    }

    @Test
    public void should_fail_calculate_price_when_rule_not_found() {
        // when
        String priceRuleName = "nonexisting";
        PriceRequest request = new PriceRequest(priceRuleName, "2011-12-03T10:15:30+01:00", "2011-12-04T10:15:30+01:00");
        ResponseEntity<String> response = testRestTemplate.postForEntity("/v1/prices", request, String.class);

        // then
        assertTrue(response.getStatusCode().is5xxServerError());
    }
}