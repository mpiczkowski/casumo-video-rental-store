package com.casumo.videorental.rental.api.v1.helpers;

import com.casumo.videorental.rental.api.v1.dto.FilmDto;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public class HelperFilmPage extends PageImpl<FilmDto> {

    @JsonCreator
    public HelperFilmPage(@JsonProperty("content") List<FilmDto> content,
                          @JsonProperty("number") int number,
                          @JsonProperty("size") int size,
                          @JsonProperty("totalElements") Long totalElements) {
        super(content, PageRequest.of(number, size), totalElements);
    }
}