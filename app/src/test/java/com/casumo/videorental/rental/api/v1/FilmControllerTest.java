package com.casumo.videorental.rental.api.v1;

import com.casumo.videorental.rental.api.v1.dto.AddFilmRequest;
import com.casumo.videorental.rental.api.v1.dto.AddResponse;
import com.casumo.videorental.rental.api.v1.dto.FilmDto;
import com.casumo.videorental.rental.api.v1.helpers.HelperFilmPage;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class FilmControllerTest {
    private static final String FILM_ITEM_UUID = UUID.randomUUID().toString();
    private static final String RENT_RULE_NAME = "any";
    public static final String FILM_TITLE = "Start Trek";
    public static final String FILM_DIRECTOR = "Robert Wise";

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    public void should_add_film() {
        // when
        ResponseEntity<AddResponse> res = addFilm(FILM_TITLE, FILM_DIRECTOR, RENT_RULE_NAME);

        // then
        assertTrue(res.getStatusCode().is2xxSuccessful());
        String newFilmId = res.getBody().getId();

        // when
        ResponseEntity<FilmDto> filmGetResponse = testRestTemplate.getForEntity("/v1/films/" + newFilmId, FilmDto.class);

        // then
        assertTrue(filmGetResponse.getStatusCode().is2xxSuccessful());
        FilmDto filmBody = filmGetResponse.getBody();
        assertEquals(newFilmId, filmBody.getFilmId());
        assertEquals(FILM_TITLE, filmBody.getTitle());
        assertTrue(filmBody.getDirectors().contains(FILM_DIRECTOR));
    }

    @Test
    public void should_fail_to_add_film_when_directors_empty() {
        // given
        ResponseEntity<AddResponse> res = testRestTemplate.postForEntity("/v1/films",
                new AddFilmRequest("K-pax", Collections.emptyList(), RENT_RULE_NAME),
                AddResponse.class);

        // then
        assertTrue(res.getStatusCode().equals(HttpStatus.BAD_REQUEST));
    }

    @Ignore //TODO: fix this test, failing on paged response
    @Test
    public void should_list_films_paged() {
        // given
        addFilm("film1", "director1", RENT_RULE_NAME);
        addFilm("film2", "director2", RENT_RULE_NAME);

        //when
        ResponseEntity<HelperFilmPage> resp = testRestTemplate.getForEntity("/v1/films", HelperFilmPage.class);

        //then
        assertTrue(resp.getStatusCode().is2xxSuccessful());
        assertEquals(1, resp.getBody().getTotalElements());
    }

    private ResponseEntity<AddResponse> addFilm(String title, String director, String rentruleName) {
        return testRestTemplate.postForEntity("/v1/films",
                new AddFilmRequest(title, Arrays.asList(director), rentruleName),
                AddResponse.class);
    }
}