package com.casumo.videorental.rental.api.v1;

import com.casumo.videorental.rental.api.v1.dto.AddFilmItemRequest;
import com.casumo.videorental.rental.api.v1.dto.AddFilmRequest;
import com.casumo.videorental.rental.api.v1.dto.AddResponse;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.UUID;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class FilmItemControllerTest {


    private static final String FILM_UUID = UUID.randomUUID().toString();
    private static final String FILM_ITEM_UUID = UUID.randomUUID().toString();

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    public void should_add_film_item() {
        // given
        ResponseEntity<AddResponse> res = testRestTemplate.postForEntity("/v1/films",
                new AddFilmRequest("title", Collections.singletonList("director"), "standard"),
                AddResponse.class);
        assertNotNull(res.getBody().getId());

        // when
        res = testRestTemplate.postForEntity("/v1/filmItems",
                new AddFilmItemRequest(res.getBody().getId(), FILM_ITEM_UUID, "any description"),
                AddResponse.class);

        // then
        assertTrue(res.getStatusCode().is2xxSuccessful());
        assertNotNull(res.getBody().getId());
    }

    @Test
    public void should_fail_when_adding_item_for_non_existing_film() {
        // when
        ResponseEntity<AddResponse> res = testRestTemplate.postForEntity("/v1/filmItems",
                new AddFilmItemRequest(FILM_UUID, FILM_ITEM_UUID, "any description"),
                AddResponse.class);

        // then
        assertTrue(res.getStatusCode().is5xxServerError());
    }

    @Ignore
    @Test
    public void should_list_film_items_by_film_id() {
    }

    @Ignore
    @Test
    public void should_return_borrowed_item() {
    }
}