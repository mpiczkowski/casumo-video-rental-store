package com.casumo.videorental.rental.api.v1;

import com.casumo.videorental.rental.api.v1.dto.AddOrderRequest;
import com.casumo.videorental.rental.api.v1.dto.AddResponse;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class OrderingProcessTest {


    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    public void should_fail_when_empty_order() {
        // when
        ResponseEntity<AddResponse> res = testRestTemplate.postForEntity("/v1/orders",
                new AddOrderRequest(Collections.emptyList()),
                AddResponse.class);

        // then
        assertTrue(res.getStatusCode().equals(HttpStatus.BAD_REQUEST));

        // when
        res = testRestTemplate.postForEntity("/v1/orders",
                new AddOrderRequest(null),
                AddResponse.class);

        // then
        assertTrue(res.getStatusCode().equals(HttpStatus.BAD_REQUEST));
    }

    @Ignore
    @Test
    public void should_fail_when_any_item_not_available() {
    }

    @Ignore
    @Test
    public void should_create_order() {
    }

    @Ignore
    @Test
    public void should_get_order_when_created() {
    }


}
