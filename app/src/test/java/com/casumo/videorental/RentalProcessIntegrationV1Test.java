package com.casumo.videorental;

import com.casumo.videorental.rental.api.v1.dto.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = DEFINED_PORT)
public class RentalProcessIntegrationV1Test {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    public void should_add_films_and_allow_ordering() {

        List<FilmDto> films = addFilms();

        List<FilmItemDto> filmItems = addFilmItems(films);

        OrderResponse order = order(filmItems);

        checkProcessedSuccessfully(order);
    }

    private void checkProcessedSuccessfully(OrderResponse order) {
        String status = order.getStatus();
        int retryCount = 5;
        do {
            OrderResponse orderResponse = getOrder(order.getId());
            status = orderResponse.getStatus();
            System.out.println("STATUS = " + status);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (!"PAID".equals(status) && retryCount-- > 0);
        assertEquals("PAID", status);
    }


    private OrderResponse getOrder(String orderId) {
        ResponseEntity<OrderResponse> resp = testRestTemplate.getForEntity("/v1/orders/" + orderId, OrderResponse.class);

        assertTrue(resp.getStatusCode().is2xxSuccessful());
        return resp.getBody();
    }

    private OrderResponse order(List<FilmItemDto> filmItems) {

        List<AddOrderRequestItem> orderRequestItems = filmItems.stream()
                .map(i -> new AddOrderRequestItem(i.getFilmId(), 2)).collect(Collectors.toList());
        ResponseEntity<OrderResponse> resp = testRestTemplate.postForEntity("/v1/orders",
                new AddOrderRequest(orderRequestItems),
                OrderResponse.class);

        assertTrue(resp.getStatusCode().is2xxSuccessful());
        OrderResponse order = resp.getBody();
        assertNotNull(order.getId());
        assertEquals("PENDING", order.getStatus());
        return order;
    }

    private List<FilmItemDto> addFilmItems(List<FilmDto> films) {
        List<String> filmItemIds = films.stream().map(this::addFilmItem).collect(Collectors.toList());

        return filmItemIds.stream().map(this::getFilmItem).collect(Collectors.toList());
    }

    private String addFilmItem(FilmDto film) {
        System.out.println(film);
        ResponseEntity<AddResponse> resp = testRestTemplate.postForEntity("/v1/filmItems",
                new AddFilmItemRequest(film.getFilmId(), UUID.randomUUID().toString(), "description"),
                AddResponse.class);

        assertTrue(resp.getStatusCode().is2xxSuccessful());
        System.out.println(resp.getBody());
        String filmItemId = resp.getBody().getId();
        assertNotNull(filmItemId);

        return filmItemId;
    }

    private FilmItemDto getFilmItem(String filmItemId) {
        ResponseEntity<FilmItemDto> resp = testRestTemplate.getForEntity("/v1/filmItems/" + filmItemId, FilmItemDto.class);

        assertTrue(resp.getStatusCode().is2xxSuccessful());
        return resp.getBody();
    }

    private List<FilmDto> addFilms() {
        String film1Id = addFilm("Tomb Raider", "Roar Uthaug", "regular");

        FilmDto film1 = getFilm(film1Id);

        String film2Id = addFilm("Matrix", "Lana Wachowski", "regular");

        FilmDto film2 = getFilm(film2Id);

        return Arrays.asList(film1, film2);
    }

    private String addFilm(String title, String director, String rentruleName) {
        ResponseEntity<AddResponse> resp = testRestTemplate.postForEntity("/v1/films",
                new AddFilmRequest(title, Arrays.asList(director), rentruleName),
                AddResponse.class);

        assertTrue(resp.getStatusCode().is2xxSuccessful());
        String filmId = resp.getBody().getId();
        assertNotNull(filmId);

        return filmId;
    }

    private FilmDto getFilm(String filmId) {
        ResponseEntity<FilmDto> resp = testRestTemplate.getForEntity("/v1/films/" + filmId, FilmDto.class);

        assertTrue(resp.getStatusCode().is2xxSuccessful());
        FilmDto film = resp.getBody();
        assertNotNull(film.getFilmId());
        return film;
    }
}
