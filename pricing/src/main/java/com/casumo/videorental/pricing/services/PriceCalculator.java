package com.casumo.videorental.pricing.services;

import com.casumo.videorental.pricing.model.PriceRule;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

@Component
public class PriceCalculator {

    /**
     * Calculates price according to rates. The minimal unit of price calculation is one day.
     * If the rent time overlaps next day the whole price for another day is calculated.
     *
     * @return calculated price
     */
    public BigDecimal calculatePrice(ZonedDateTime rentStartDate, ZonedDateTime rentEndDate, PriceRule priceRule) {
        long exactDaysCount = ChronoUnit.DAYS.between(rentStartDate, rentEndDate);
        long daysCount = exactDaysCount + (isOverlappingNextDay(exactDaysCount, rentStartDate, rentEndDate) ? 1 : 0);
        long remainingDays = daysCount - priceRule.getFirstDays();

        BigDecimal price = priceRule.getFirstRate().getAmount();

        if (remainingDays <= 0) {
            return price;

        } else {
            return price.add(priceRule.getOtherRate().getAmount().multiply(new BigDecimal(remainingDays)));
        }
    }

    private boolean isOverlappingNextDay(long exactDaysCount, ZonedDateTime rentStartDate, ZonedDateTime rentEndDate) {
        ZonedDateTime exactEndTime = rentStartDate.plus(exactDaysCount, ChronoUnit.DAYS);
        Duration overlapedDayDuration = Duration.between(exactEndTime, rentEndDate);
        return overlapedDayDuration.getSeconds() > 0;
    }

}
