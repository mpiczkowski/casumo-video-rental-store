package com.casumo.videorental.pricing.services;

import com.casumo.videorental.pricing.api.v1.dto.PriceBulkRequest;
import com.casumo.videorental.pricing.api.v1.dto.PriceBulkRequestEntry;
import com.casumo.videorental.pricing.api.v1.dto.PriceBulkResponse;
import com.casumo.videorental.pricing.api.v1.dto.PriceBulkResponseEntry;
import com.casumo.videorental.pricing.exceptions.PriceRuleNotFoundException;
import com.casumo.videorental.pricing.model.PriceRule;
import com.casumo.videorental.pricing.repository.PriceRuleRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PricingService {

    private final PriceRuleRepository priceRuleRepository;
    private final PriceCalculator priceCalculator;

    public PricingService(PriceRuleRepository priceRuleRepository, PriceCalculator priceCalculator) {
        this.priceRuleRepository = priceRuleRepository;
        this.priceCalculator = priceCalculator;
    }

    /**
     * Gets price according to rates. The minimal unit of price calculation is one day.
     * If the rent time overlaps next day the whole price for another day is calculated.
     * @param priceBulkRequest a bulk request containing list of items to calculate prices for
     * @return calculated price
     */
    public PriceBulkResponse getPrice(PriceBulkRequest priceBulkRequest) {
        List<PriceBulkResponseEntry> calculatedItems = priceBulkRequest.getItems().stream()
                .map(this::getPriceBulkResponseEntry)
                .collect(Collectors.toList());
        BigDecimal total = calculatedItems.stream()
                .map(i -> i.getPrice())
                .reduce((a, b) -> a.add(b)).orElse(BigDecimal.ZERO);
        return new PriceBulkResponse(calculatedItems, total);
    }


    /**
     * Gets price according to rates. The minimal unit of price calculation is one day.
     * If the rent time overlaps next day the whole price for another day is calculated.
     * @param rentStartTime a start time of the rent
     * @param rentEndTime an end time of the rent
     * @param priceRuleName a name of rule according to which price is calculated
     *
     * @return calculated price
     */
    public BigDecimal getPrice(ZonedDateTime rentStartTime, ZonedDateTime rentEndTime, String priceRuleName) {
        PriceRule priceRule = priceRuleRepository.findById(priceRuleName)
                .orElseThrow(() -> new PriceRuleNotFoundException("Could not find price rule with name: " + priceRuleName));

        return priceCalculator.calculatePrice(rentStartTime, rentEndTime, priceRule);
    }


    private PriceBulkResponseEntry getPriceBulkResponseEntry(PriceBulkRequestEntry priceBulkRequestEntry) {
        BigDecimal price = this.getPrice(priceBulkRequestEntry.getStart(), priceBulkRequestEntry.getEnd(), priceBulkRequestEntry.getRule());
        return new PriceBulkResponseEntry(priceBulkRequestEntry.getUuid(), price);
    }
}
