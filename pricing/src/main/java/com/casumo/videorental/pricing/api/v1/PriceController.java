package com.casumo.videorental.pricing.api.v1;

import com.casumo.videorental.pricing.api.v1.dto.PriceBulkRequest;
import com.casumo.videorental.pricing.api.v1.dto.PriceBulkResponse;
import com.casumo.videorental.pricing.api.v1.dto.PriceRequest;
import com.casumo.videorental.pricing.api.v1.dto.PriceResponse;
import com.casumo.videorental.pricing.services.PricingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequestMapping(path = "/v1")
public class PriceController {

    private final PricingService pricingService;

    @Autowired
    public PriceController(PricingService pricingService) {
        this.pricingService = pricingService;
    }

    @RequestMapping(path = "/prices", method = RequestMethod.POST)
    public ResponseEntity<PriceResponse> getPriceAmount(@RequestBody PriceRequest priceRequest) {
        BigDecimal price = pricingService.getPrice(priceRequest.getStart(), priceRequest.getEnd(), priceRequest.getRule());
        return new ResponseEntity<>(new PriceResponse(price), HttpStatus.OK);
    }

    @RequestMapping(path = "/prices/bulk", method = RequestMethod.POST)
    public ResponseEntity<PriceBulkResponse> getBulkPrice(@RequestBody PriceBulkRequest priceBulkRequest) {
        return new ResponseEntity<>(pricingService.getPrice(priceBulkRequest), HttpStatus.OK);
    }
}
