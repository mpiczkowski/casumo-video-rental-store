package com.casumo.videorental.pricing.repository;

import com.casumo.videorental.pricing.model.PriceRate;
import com.casumo.videorental.pricing.model.PriceRule;
import org.springframework.stereotype.Component;

@Component
public class PriceRatesGenerator {

    private final PriceRuleRepository priceRuleRepository;
    private final PriceRateRepository priceRateRepository;

    public PriceRatesGenerator(PriceRuleRepository priceRuleRepository, PriceRateRepository priceRateRepository) {
        this.priceRuleRepository = priceRuleRepository;
        this.priceRateRepository = priceRateRepository;
    }

    /**
     * Generate default price rules and rates for pricing calculations
     */
    public void generate() {
        generatePriceRates();
        generatePriceRules();
    }

    private void generatePriceRules() {
        generatePriceRule(PriceRule.NEW_RELEASES);
        generatePriceRule(PriceRule.REGULAR);
        generatePriceRule(PriceRule.OLD);

    }

    private void generatePriceRates() {
        generatePriceRate(PriceRate.BASIC);
        generatePriceRate(PriceRate.PREMIUM);
        generatePriceRate(PriceRate.PREMIUM);

    }

    private void generatePriceRate(PriceRate rate) {
        priceRateRepository.findById(rate.getName())
                .orElseGet(() -> priceRateRepository.save(rate));
    }

    private void generatePriceRule(PriceRule rule) {
        priceRuleRepository.findById(rule.getName())
                .orElseGet(() -> priceRuleRepository.save(rule));
    }
}

