package com.casumo.videorental.pricing.repository;

import com.casumo.videorental.pricing.model.PriceRule;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PriceRuleRepository extends MongoRepository<PriceRule, String> {
}
