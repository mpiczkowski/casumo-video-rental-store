package com.casumo.videorental.pricing.exceptions;

public class PriceRuleNotFoundException extends RuntimeException {
    public PriceRuleNotFoundException(String message) {
        super(message);
    }
}
