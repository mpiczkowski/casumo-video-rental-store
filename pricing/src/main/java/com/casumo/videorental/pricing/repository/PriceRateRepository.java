package com.casumo.videorental.pricing.repository;

import com.casumo.videorental.pricing.model.PriceRate;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PriceRateRepository extends MongoRepository<PriceRate, String> {
}
