package com.casumo.videorental.pricing.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import static com.casumo.videorental.pricing.model.PriceRate.BASIC;
import static com.casumo.videorental.pricing.model.PriceRate.PREMIUM;

@Document(collection = "pricing_price_rules")
@AllArgsConstructor
@Getter
public class PriceRule {

    public static final PriceRule NEW_RELEASES = new PriceRule("new releases", PREMIUM, 0, PREMIUM);
    public static final PriceRule REGULAR = new PriceRule("regular", BASIC, 3, BASIC);
    public static final PriceRule OLD = new PriceRule("old", BASIC, 5, BASIC);

    @Id
    private final String name;

    private final PriceRate firstRate;
    private final long firstDays;
    private final PriceRate otherRate;

}
