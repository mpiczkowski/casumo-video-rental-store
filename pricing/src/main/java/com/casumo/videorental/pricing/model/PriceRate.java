package com.casumo.videorental.pricing.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Document(collection = "pricing_price_rates")
@AllArgsConstructor
@Getter
public class PriceRate {

    public static final PriceRate PREMIUM = new PriceRate("premium", BigDecimal.valueOf(40));
    public static final PriceRate BASIC = new PriceRate("basic", BigDecimal.valueOf(30));

    @Id
    private final String name;
    private final BigDecimal amount;

}
