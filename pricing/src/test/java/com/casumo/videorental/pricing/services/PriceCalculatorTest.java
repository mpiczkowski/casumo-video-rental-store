package com.casumo.videorental.pricing.services;

import com.casumo.videorental.pricing.model.PriceRate;
import com.casumo.videorental.pricing.model.PriceRule;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Collection;

import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;
import static java.util.Arrays.asList;

@RunWith(Parameterized.class)
public class PriceCalculatorTest {

    private final BigDecimal firstDaysRate;
    private final long firstDaysCount;
    private final BigDecimal remainingDaysRate;
    private final ZonedDateTime start;
    private final ZonedDateTime end;
    private final BigDecimal expectedPrice;

    private PriceCalculator pricingService = new PriceCalculator();

    public PriceCalculatorTest(double firstDaysRate, long firstDaysCount, double remainingDaysRate, String start, String end, double expectedPrice) {
        this.firstDaysRate = BigDecimal.valueOf(firstDaysRate);
        this.firstDaysCount = firstDaysCount;
        this.remainingDaysRate = BigDecimal.valueOf(remainingDaysRate);
        this.start = ZonedDateTime.from(ISO_DATE_TIME.parse(start));
        this.end = ZonedDateTime.from(ISO_DATE_TIME.parse(end));
        this.expectedPrice = BigDecimal.valueOf(expectedPrice);
    }

    @Parameterized.Parameters(name = "{index}: firstDaysRate={0}, firstDaysCount={1}, remainingDaysRate={2}, start={3}, end={4}")
    public static Collection<Object[]> data() {
        return asList(new Object[][]{
                {0, 0, 40, "2011-12-03T10:15:30+01:00", "2011-12-04T10:15:30+01:00", 40}, // 1 day
                {0, 0, 40, "2011-12-03T10:15:30+02:00", "2011-12-04T12:15:30+03:00", 80}, // started 2nd day
                {30, 3, 30, "2012-01-01T15:00:00+01:00", "2012-01-03T15:00:00+01:00", 30}, // 2 days
                {30, 3, 30, "2012-01-01T15:00:00+01:00", "2012-01-06T15:00:00+01:00", 90}, // 5 days
                {30, 3, 30, "2012-01-01T15:00:00+01:00", "2012-01-04T15:00:00+01:00", 30}, // 3 days
                {30, 5, 30, "2012-01-01T15:00:00+01:00", "2012-01-08T15:00:00+01:00", 90}, // 7 days
        });
    }

    @Test
    public void testCalculatePrice() {
        // given
        PriceRule priceRule = new PriceRule("x", new PriceRate("r1", firstDaysRate), firstDaysCount, new PriceRate("r2", remainingDaysRate));

        // then
        Assert.assertEquals(expectedPrice, pricingService.calculatePrice(start, end, priceRule));

    }


}
