package com.casumo.videorental.gamification.repository;

import com.casumo.videorental.gamification.model.CustomerBonusPoints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public class CustomerBonusPointsRepositoryImpl implements CustomerBonusPointsRepositoryEx {

    private final MongoTemplate mongoTemplate;

    @Autowired
    public CustomerBonusPointsRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    /**
     * Increments customer bonus points with given amount
     *
     * @param borrowerId a customer who borrowed a film
     * @param points     points to be added to customer bonus points
     */
    @Override
    public void incrementPoints(String borrowerId, int points) {
        Query query = Query.query(Criteria.where("borrowerId").is(borrowerId));
        Update incrUpdate = new Update().inc("points", points).setOnInsert("borrowerId", borrowerId);
        mongoTemplate.upsert(query, incrUpdate, CustomerBonusPoints.class);
    }

}
