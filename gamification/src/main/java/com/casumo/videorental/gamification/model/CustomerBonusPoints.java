package com.casumo.videorental.gamification.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "gamification_customer_bonus_points")
@AllArgsConstructor
@Getter
public class CustomerBonusPoints {
    @Id
    private final String borrowerId;
    private final int points;
}
