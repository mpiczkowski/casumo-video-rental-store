package com.casumo.videorental.gamification.repository;

import com.casumo.videorental.gamification.model.CustomerBonusPoints;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerBonusPointsRepository extends MongoRepository<CustomerBonusPoints, String>, CustomerBonusPointsRepositoryEx {
}
