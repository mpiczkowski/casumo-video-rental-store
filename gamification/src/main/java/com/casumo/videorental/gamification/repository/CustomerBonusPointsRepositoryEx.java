package com.casumo.videorental.gamification.repository;

public interface CustomerBonusPointsRepositoryEx {
    void incrementPoints(String borrowerId, int points);
}
