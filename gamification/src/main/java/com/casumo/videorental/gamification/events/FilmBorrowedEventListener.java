package com.casumo.videorental.gamification.events;

import com.casumo.videorental.gamification.repository.CustomerBonusPointsRepository;
import com.casumo.videorental.rental.events.FilmBorrowedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class FilmBorrowedEventListener {

    public static final String FILM_CATEGORY_NEW_RELEASES = "new releases";
    private final CustomerBonusPointsRepository customerBonusPointsRepository;

    public FilmBorrowedEventListener(CustomerBonusPointsRepository customerBonusPointsRepository) {
        this.customerBonusPointsRepository = customerBonusPointsRepository;
    }

    @EventListener
    public void handleEvent(FilmBorrowedEvent event) {
        log.info("Handling event " + event.getClass().getName());
        String borrowerId = event.getBorrowerId();
        String filmCategory = event.getFilmCategory();
        int points = 0;
        switch (filmCategory) {
            case FILM_CATEGORY_NEW_RELEASES:
                points = 2;
                break;
            default:
                points = 1;
        }
        customerBonusPointsRepository.incrementPoints(borrowerId, points);
    }

}