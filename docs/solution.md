# Design considerations
## 10000 feet look at the system

The first question I asked myself when looking at the system from 1.000 feet height was which architectural approach I should take and where I would deploy the application.

Should it run in public cloud on cloud provider infrastructure, e.g on Amazon, Azure, DigitalOcean, etc.

Should we use services from the provider (e.g. DynamoDB or SQS from AWS) or should we omit vendor locking and e.g. deploy application within docker containers managed by Kubernetes.

As I do not know answers to the above questions I need to do some assumptions.

I am assuming that:

* I can build the whole infrastructure from scratch and will not make it dependent from any particular cloud provider.

* The application should be cloud ready, as this is a standard nowadays and I think the cost of creating cloud native application from
scratch is relatively smaller than refactoring existing monolith which was built without "cloud ready" in mind.

*  The technologies are changing quickly and I want the application to live long and follow the changes. Therefore it must be designed with abstractions allowing to easily switch the frameworks used, e.g. database, REST controllers, adapters to external systems.

### Cloud ready
It means the application is built using a set of standard techniques often called ["12-factor methodologies"](https://12factor.net/)

To make development easier I decided to choose Spring Cloud libraries which like other Spring Boot libraries base on the principle of convention over configuration.

This may often sound for someone not familiar with the libraries that magic happens because no configuration can be found in the project but still it works :)
I am personally not a fan of the magic happening in project because it is often difficult to track errors, but using some of the facilities helps to speed up
development and reduce risk of introducing new bugs if we tried to "reinvent the wheel".
Spring libraries are battle tested and open source so any bugs are fixed by community (posiblly quicker than if we tried to develop the same features alone).

At this point I don't know yet how many services I'm going to build. We can start from building a monolith but do it right, so that we don't create 
a "big ball of mud" when everyting is connected with everything else, hard to refactor and difficult to understand.
Then if necessary we can easily extract each module into separate microservice.
There is a [good article](https://content.pivotal.io/blog/should-that-be-a-microservice-keep-these-six-factors-in-mind?utm_campaign=content-social&utm_medium=social-sprout&utm_source=twitter&utm_content=1516394228) by Nate Shutta about decision factors when to decompose monolyth into microservices.

### Domain Driven Design
The next thing we need to do is to analise the domain of our system and distinguish boundaries.

There is a technique known as Domain Driven Design (DDD) which helps to identify so called [bounded contexts](https://martinfowler.com/bliki/BoundedContext.html) of the system. As a result, each of the contexts can form a separate module.

In DDD there is a specific nomenclature, which I'm not going to explain extensively here.

There are better places to visit and understand the terminology behind DDD, like [this one](http://dddcommunity.org/resources/ddd_terms/).
DDD helps to create architecture called Hexagonal Architecture (also called Ports and Adapters architecture) which wraps domain in the core of the system and adds additional layers on top of it (like ports and adapters) allowing us to easily switch framework without necessity to rewrite core business logic but most importantly it allows what Alister Cocbourn wrote: 

*Allow an application to equally be driven by users, programs, automated test or batch scripts, and to be developed and tested in isolation from its eventual run-time devices and databases. As events arrive from the outside world at a port, a technology-specific adapter converts it into a usable procedure call or message and passes it to the application. The application is blissfully ignorant of the nature of the input device. When the application has something to send out, it sends it out through a port to an adapter, which creates the appropriate signals needed by the receiving technology (human or automated). The application has a semantically sound interaction with the adapters on all sides of it, without actually knowing the nature of the things on the other side of the adapters.*

In our case it would allow us to start with monolith and then in any case switch to microservices, or e.g. easily swap relational database with NoSQL database, etc. More about benefits of such architecture can be found [here](http://alistair.cockburn.us/Hexagonal+architecture)

### Event Storming
A useful technique to work on establishing the context is called [Event Storming](https://en.wikipedia.org/wiki/Event_storming) and although I haven't had chance to use it yet in real project, this task is a good reason for trying it out.

I found a nice step-by-step walk-through the process [here](https://spring.io/blog/2018/04/11/event-storming-and-spring-with-a-splash-of-ddd).

Following the process for our case led me to the following results:

![Generating project](images/event_storming.png)
*(done with online tool [pinup.com](https://pinup.com/SJGLOQAsM))*


I have identified the following modules:

* user module, which will be used to manage users as well as for their authentication and authorization

* payment module, which will manage financial transactions for film rentals such as payments, refunds, etc.

* pricing module, which will manage price rules and calculate rental cost.

* rental module, which will be used to add new films for rent, retire old films or broken items, let users borrow and return the films.

* gamification module which for now will only track customer bonus points


I agree with the statement explained [here](https://github.com/ddd-by-examples/factory/blob/master/README.md) that "Not every piece of software is equally important" and I asked myself a question which of the modules above is the most important.

Of these modules the most complex domain seems to belong to rental service and my initial attempt was to use DDD and create hexagonal architecture for it, but when I started and saw the additional workload which it would cost me and compared it with available time for this project, I postponed it.

After all this was supposed to be "not so intimidating" task.

Finally, I used simple Create Read Update Delete architecture pattern.

### System description
I started with the rental, pricing and gamification modules but have not finished them. 

Rather I created a shape of project which can be continued later in the same manner which I'm going to explain here. 

Similarly, the user and payment modules will be the subject of future work.

I started with generating the application from http://start.spring.io.
![Generating project](images/generating_project.png)


Then created modules and packages.


app : com.casumo.videorental

rental :  com.casumo.videorental.pricing

pricing : com.casumo.videorental.pricing

gamification : com.casumo.videorental.gamification

To simplify code I use [Lombok](https://projectlombok.org/).


Along the way, when reuse of REST APIs and events APIs was required among multiple modules, I extracted new modules:

pricing-api

rental-events

### Model
`Film` - is a definition of film details such as title and directors and a `PriceRule` name applicable when renting `FilmItems`.

`FilmItem` - is a representation of unique physical film e.g. a DVD box. The way how particular items on store are represented depends on how store works. It could be e.g. an electronic chip assigned to each borrowed film, or a bar code on DVD box. When `FilmItem` is borrowed it has added `RentInfo` which keeps borrower ID, rent period and order ID.

`Order` - keeps information about films ordered by user at certain point of time. Useful to track which particular `FilmItem`s were ordered together and when. It may have a link to payment invoice in future.

`PriceRule` - is a definition of `PriceRate` for particular number of first days (if any), plus `PriceRate` for each remaining day.

`PriceRate` - is just a label for particular amount

#### How renting works
I've drawn a simple sequence diagram of the renting scenario.

![Generating project](images/renting_sequence_diagram.png)

*(done with online tool [lucidchart.com](https://www.lucidchart.com)*

When user orders films  for rent (`POST /orders`) the request need to contain a list of pairs: 

* film identifier 
* for how many days it has to be borrowed 

The system will then find if there are particular films available on stock which are not already borrowed by other users.

As we cannot predict whether other users will return films on time, we only can borrow a film if it is not currently borrowed.

If the order cannot be created the system will return error, otherwise it would return the order unique identifier (UUID).


When order is created, an event is sent which can be processed asynchronously as long as all data necessary for payment is already in the system.

For the sake of simplicity I assumed this is the case.

Then the system could get the price from Pricing Service (`POST /prices/bulk`), connect to Payment Service and execute payments.


From the requirements it is not clear if the payment needs to be done synchronously when user sends order.

E.g. if the user does no have a credit card registered in the system but needs to be redirected to a bank to execute transfer then it would need to be run synchronously (e.g. as a series of redirects).

In case when user has card registered then the system can automatically charge user. 


The Pricing Service endpoint should be able to return total price for the whole order. It would be more efficient then querying it separately for each item. 

Still it would be useful to have API allowing to check price for single item (`POST /prices`), e.g. when it is needed to calculate surcharges when user returned items later than initially paid for.


If the payment fails then the Order status is updated with PAYMENT_FAILED and we could e.g. add a periodic job to repeat payment before we set status to FAILED after a series of retries (not implemented yet).



If the payment succeeds the Order status will be PAID.

At any moment user could check the status of Order (`GET /orders/{order id}`).

User can return particular films separately. It would be useful to track when the item is returned, e.g. to resolve any disputes in cases when the user claims returning the film but in fact the film is missing.

We could store an event in system about each returned film.

In addition to ordering items there should be API allowing to add new films on stock or remove damaged or "retired" items from stock.

#### REST APIs
The final list of APIs and example usage with [curl](https://pl.wikipedia.org/wiki/CURL) tool is in main [README](../README.md) document.

The APIs are versioned using URL schema, e.g.: `GET /v1/films`

#### Domain objects lifecycle

##### Order

![Generating project](images/order_lifecycle.png)

*(done with online tool [lucidchart.com](https://www.lucidchart.com)*

##### FilmItem

![Generating project](images/film_item_lifecycle.png)

*(done with online tool [lucidchart.com](https://www.lucidchart.com)*

#### Storage
MongoDB is used as storage. I use free MongoDB hosting from [mLab](https://mlab.com/)
 
Why not SQL database? 

Because I want the application to scale easily from the beginning and to be able to store complex structures in one record (e.g. Order with all ordered items embedded in it).

It could be any other NoSQL database but I think MongoDB fits well.

I want the system to be eventually consistent so I do not use ACID transactions which cost performance.

E.g. When `FilmItem`s are booked we could do it in the same transaction in which Order is created.

Then all these `FilmItem`s cannot be also booked by other users, which is correct, but adds additional overhead on database, which has to manage it.

It is also easy to do it wrong (e.g. using READ_COMMITTED transaction isolation level) and lock entire `FilmItem`s table for read when Order is created and some `FilmItem`s has just been updated but transaction not committed yet..


Instead, I do not use transaction and have to manually manage consistency of data. 

I do it [this way](https://bitbucket.org/mpiczkowski/casumo-video-rental-store/src/005d682a84831328313d1f06309f03090f57a2e2/rental/src/main/java/com/casumo/videorental/rental/services/OrderService.java?at=master&fileviewer=file-view-default#OrderService.java-87): 

1. Update all required `FilmItem`s which are in `ACTIVE` state to `BOOKED` state and set `rentInfo`.

2. Compare the number of updated items with the number of films in `Order`.

If the numbers match, then all went well. Otherwise one of the films was not available (e.g. last film item on stock was just borrowed by another user)
and the previous operation needs to be rolled back. I will update all `BOOKED` items in step 1 to `AVAILABLE` again and clean rentInfo.

It is still possible that in rare cases items can be booked but order not created. 
To bring back consistent system state we could add a scheduled job which checks if BOOKED items have corresponding orders.

I leave it out of scope of the current implementation.

When items are BORROWED an event is propagated. It is handled in gamification module to increment customer bonus points.


I've added an integration test `RentalProcessIntegrationV1Test` [here](https://bitbucket.org/mpiczkowski/casumo-video-rental-store/src/005d682a84831328313d1f06309f03090f57a2e2/app/src/test/java/com/casumo/videorental/RentalProcessIntegrationV1Test.java?at=master&fileviewer=file-view-default) which tests whole end-to-end rental process.


The API layer is separated from model layer (separate set of data transfer objects translated from/to model objects).

This way I can use a common API classes in other modules which would like to call the API (e.g. rental module using pricing API).

