package com.casumo.videorental.rental.repository;


import com.casumo.videorental.rental.model.FilmItem;
import com.casumo.videorental.rental.model.FilmItemState;
import com.casumo.videorental.rental.model.RentInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.*;

/**
 * Integration Test for {@link FilmRepository}.
 */
@RunWith(SpringRunner.class)
@DataMongoTest
public class FilmItemRepositoryIntegrationTest {

    @Autowired
    private FilmItemRepository filmItemRepository;

    @Test
    public void should_find_by_id_and_state_and_update() {
        // given
        String filmId = UUID.randomUUID().toString();
        String filmItemId = UUID.randomUUID().toString();
        String borrowerId = UUID.randomUUID().toString();
        Instant now = Instant.now();
        RentInfo rentInfo = new RentInfo(borrowerId, now.minus(1, ChronoUnit.DAYS), now, null);
        FilmItem saved = filmItemRepository.save(new FilmItem(filmItemId, filmId, "new film item", FilmItemState.BORROWED, rentInfo));

        // when
        Optional<FilmItem> updated = filmItemRepository.findByIdAndStateAndModify(saved.getId(), FilmItemState.BORROWED, FilmItemState.RETIRED, null);

        // then
        assertNotNull(updated);
        assertTrue(updated.isPresent());
        assertEquals(FilmItemState.RETIRED, updated.get().getState());
        assertNull(updated.get().getRentInfo());
    }

    @Test
    public void should_not_update_by_id_and_state_when_nothing_found_for_update() {
        // when
        Optional<FilmItem> updated = filmItemRepository.findByIdAndStateAndModify(UUID.randomUUID().toString(), FilmItemState.BORROWED, FilmItemState.RETIRED, null);

        // then
        assertNotNull(updated);
        assertFalse(updated.isPresent());
    }

}