package com.casumo.videorental.rental.repository;


import com.casumo.videorental.rental.model.Film;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.*;

/**
 * Integration Test for {@link FilmRepository}.
 */
@RunWith(SpringRunner.class)
@DataMongoTest
public class FilmRepositoryIntegrationTest {

    private static final String FILM_TITLE = "Matrix";
    private static final String DIRECTOR_1 = "Lana Wachowski";

    @Autowired
    private FilmRepository filmRepository;

    @Test
    public void should_find_saved_film_by_id() {
        String filmId = UUID.randomUUID().toString();
        Film saved = filmRepository.save(new Film(filmId, FILM_TITLE, Arrays.asList(DIRECTOR_1, "Lilly Wachowski"), "standard"));

        Optional<Film> found = filmRepository.findById(saved.getId());

        assertNotNull(found);
        assertTrue(found.isPresent());
        assertEquals(FILM_TITLE, found.get().getTitle());
        assertTrue(found.get().getDirectors().contains(DIRECTOR_1));
    }

}