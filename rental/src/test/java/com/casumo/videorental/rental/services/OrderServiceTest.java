package com.casumo.videorental.rental.services;

import com.casumo.videorental.rental.api.v1.dto.AddOrderRequest;
import com.casumo.videorental.rental.api.v1.dto.AddOrderRequestItem;
import com.casumo.videorental.rental.exceptions.FilmUnavailableException;
import com.casumo.videorental.rental.model.FilmItem;
import com.casumo.videorental.rental.model.FilmItemState;
import com.casumo.videorental.rental.model.RentInfo;
import com.casumo.videorental.rental.repository.FilmItemRepository;
import com.casumo.videorental.rental.repository.FilmRepository;
import com.casumo.videorental.rental.repository.OrderRepository;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {

    @Mock
    private OrderRepository orderRepository;
    @Mock
    private FilmItemRepository filmItemRepository;
    @Mock
    private FilmRepository filmRepository;
    @Mock
    private ApplicationEventPublisher eventPublisher;
    @Mock
    private PricingClient pricingClient;
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    private OrderService orderService;

    @Before
    public void setup() {
        orderService = new OrderService(orderRepository, filmItemRepository, filmRepository, eventPublisher, pricingClient);
    }

    @Test
    public void should_not_create_order_when_no_items() {
        // then
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Order cannot be empty");

        // when
        orderService.createNewOrder(new AddOrderRequest(Collections.emptyList()));
    }

    @Test
    public void should_rollback_order_when_any_item_not_available() {
        // given
        String filmId = "anyId_1";
        int numOfDays = 2;
        AddOrderRequestItem item = new AddOrderRequestItem(filmId, numOfDays);
        when(filmItemRepository.findByFilmIdAndStateAndModify(eq(item.getFilmId()), eq(FilmItemState.ACTIVE), eq(FilmItemState.BOOKED), any()))
                .thenReturn(Optional.empty());

        FilmItem filmItemToRollBack = new FilmItem("anyId_2", "anyFilmId", "description", FilmItemState.BOOKED, getDummyRentInfo());
        when(filmItemRepository.findAllByOrderId(any())).thenReturn(Arrays.asList(filmItemToRollBack));
        ArgumentCaptor<FilmItem> argumentCaptor = ArgumentCaptor.forClass(FilmItem.class);
        when(filmItemRepository.save(argumentCaptor.capture())).thenReturn(filmItemToRollBack);

        // then
        expectedEx.expect(FilmUnavailableException.class);
        expectedEx.expectMessage("One of the films in order is no longer available.");

        // when
        orderService.createNewOrder(new AddOrderRequest(Arrays.asList(item)));

        // then
        FilmItem rolledBack = argumentCaptor.getValue();
        assertEquals(FilmItemState.ACTIVE, rolledBack.getState());
        assertNull(rolledBack.getRentInfo());
    }

    @Ignore
    @Test
    public void should_create_order_and_propagate_event(){

    }

    private RentInfo getDummyRentInfo() {
        return new RentInfo("borrowerId", Instant.now(), Instant.now().plus(2, ChronoUnit.DAYS), "anyOrderId");
    }
}
