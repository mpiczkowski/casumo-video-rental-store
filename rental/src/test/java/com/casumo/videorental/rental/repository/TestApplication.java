package com.casumo.videorental.rental.repository;

import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Test application context required for integration tests in this module.
 */
@SpringBootApplication
public class TestApplication {
}