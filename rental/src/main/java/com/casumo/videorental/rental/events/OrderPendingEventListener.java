package com.casumo.videorental.rental.events;

import com.casumo.videorental.rental.services.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class OrderPendingEventListener {

    private final OrderService orderService;

    public OrderPendingEventListener(OrderService orderService) {
        this.orderService = orderService;
    }

    @EventListener
    public void handleEvent(OrderPendingEvent event) {
        log.info("Order created, id: {}", event.getOrderId());
        orderService.processOrder(event.getOrderId());
    }

}