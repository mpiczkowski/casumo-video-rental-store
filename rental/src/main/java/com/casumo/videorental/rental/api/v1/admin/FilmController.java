package com.casumo.videorental.rental.api.v1.admin;

import com.casumo.videorental.rental.api.v1.dto.AddFilmRequest;
import com.casumo.videorental.rental.services.FilmService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Admin API to manage films in store
 */
@RestController
@RequestMapping(path = "/v1")
@Slf4j
public class FilmController {

    private final FilmService filmService;

    @Autowired
    public FilmController(FilmService filmService) {
        this.filmService = filmService;
    }

    @RequestMapping(path = "/films", method = RequestMethod.POST)
    public ResponseEntity addFilm(@RequestBody @Valid AddFilmRequest addFilmRequest) {
        return new ResponseEntity(filmService.add(addFilmRequest), HttpStatus.OK);
    }

    @RequestMapping(path = "/films", method = RequestMethod.GET)
    public ResponseEntity getFilms(
            @PageableDefault(sort = {"title"}) Pageable pageable) {
        log.debug("Page {}, size {}, sort {}", pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort());
        return new ResponseEntity<>(filmService.findAll(pageable), HttpStatus.OK);
    }

    @RequestMapping(path = "/films/{id}", method = RequestMethod.GET)
    public ResponseEntity getFilm(@PathVariable("id") String id) {
        return new ResponseEntity<>(filmService.findById(id), HttpStatus.OK);
    }

}
