package com.casumo.videorental.rental.repository;

import com.casumo.videorental.rental.model.Film;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FilmRepository extends MongoRepository<Film, String> {
    Optional<Film> findByTitle(String title);
}
