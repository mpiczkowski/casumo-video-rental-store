package com.casumo.videorental.rental.exceptions;

public class FilmUnavailableException extends RuntimeException {
    public FilmUnavailableException(String message){
        super(message);
    }
}
