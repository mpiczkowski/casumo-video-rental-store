package com.casumo.videorental.rental.api.v1.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor(onConstructor = @__(@JsonCreator))
@Getter
public class FilmDto {
    @JsonProperty(required = true)
    private final String filmId;
    @JsonProperty(required = true)
    private final String title;
    @JsonProperty(required = true)
    private final List<String> directors;
    private final String priceRuleName;
}
