package com.casumo.videorental.rental.api.v1.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@AllArgsConstructor(onConstructor = @__(@JsonCreator))
@Getter
public class AddOrderRequest {
    @NotEmpty
    private final List<AddOrderRequestItem> items;
}
