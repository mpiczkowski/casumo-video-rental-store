package com.casumo.videorental.rental.model;

public enum OrderState {
    PENDING,
    PAYMENT_PENDING,
    PAID,
    PAYMENT_FAILED,
    ERROR
}
