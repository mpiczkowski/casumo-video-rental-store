package com.casumo.videorental.rental.converters;

import com.casumo.videorental.rental.api.v1.dto.FilmItemDto;
import com.casumo.videorental.rental.api.v1.dto.RentInfoDto;
import com.casumo.videorental.rental.model.FilmItem;
import com.casumo.videorental.rental.model.RentInfo;
import org.springframework.stereotype.Component;

@Component
public class FilmItemConverter {

    public FilmItemDto from(FilmItem filmItem) {
        return new FilmItemDto(filmItem.getFilmId(), filmItem.getId(), filmItem.getDescription(), from(filmItem.getRentInfo()));
    }

    private RentInfoDto from(RentInfo rentInfo) {
        return rentInfo == null ? null : new RentInfoDto(rentInfo.getFrom(), rentInfo.getTo());
    }
}
