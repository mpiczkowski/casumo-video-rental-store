package com.casumo.videorental.rental.exceptions;

public class NoSuchFilmException extends RuntimeException {
    public NoSuchFilmException(String message) {
        super(message);
    }
}
