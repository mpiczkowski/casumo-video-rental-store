package com.casumo.videorental.rental.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "rental_films")
@AllArgsConstructor
@Builder(toBuilder = true)
@Getter
public class Film {

    @Id
    private String id;
    private String title;
    private List<String> directors;
    private String priceRuleId;
}
