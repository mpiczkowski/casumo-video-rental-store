package com.casumo.videorental.rental.repository;

import com.casumo.videorental.rental.model.FilmItem;
import com.casumo.videorental.rental.model.FilmItemState;
import com.casumo.videorental.rental.model.RentInfo;

import java.util.Optional;

public interface FilmItemRepositoryEx {

    Optional<FilmItem> findByIdAndStateAndModify(String filmItemId, FilmItemState currentState, FilmItemState updatedState, RentInfo updatedRentInfo);
    Optional<FilmItem> findByFilmIdAndStateAndModify(String filmId, FilmItemState currentState, FilmItemState updatedState, RentInfo updatedRentInfo);
}
