package com.casumo.videorental.rental.api.v1.dto;

import lombok.Value;

@Value
public class AddResponse {
    private final String id;
}
