package com.casumo.videorental.rental.model;

public enum FilmItemState {
    ACTIVE,
    RETIRED,
    BORROWED,
    BOOKED
}

