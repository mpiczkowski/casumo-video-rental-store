package com.casumo.videorental.rental.api.v1.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@AllArgsConstructor(onConstructor = @__(@JsonCreator))
@Getter
public class AddFilmRequest {
    @JsonProperty(required = true)
    private final String title;
    @NotEmpty
    @JsonProperty(required = true)
    private final List<String> directors;
    @JsonProperty(required = true)
    private final String priceRuleName;

}
