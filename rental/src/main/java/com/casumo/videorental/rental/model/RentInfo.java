package com.casumo.videorental.rental.model;

import lombok.Value;

import java.time.Instant;

@Value
public class RentInfo {
    private final String borrowerId;
    private final Instant from;
    private final Instant to;
    private final String orderId;
}
