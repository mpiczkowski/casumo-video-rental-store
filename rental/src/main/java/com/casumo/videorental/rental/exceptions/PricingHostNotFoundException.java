package com.casumo.videorental.rental.exceptions;

public class PricingHostNotFoundException extends RuntimeException {
    public PricingHostNotFoundException(String message) {
        super(message);
    }
}
