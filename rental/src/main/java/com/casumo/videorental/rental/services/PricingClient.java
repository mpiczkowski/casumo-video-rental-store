package com.casumo.videorental.rental.services;

import com.casumo.videorental.pricing.api.v1.dto.*;
import com.casumo.videorental.rental.exceptions.NoPriceRuleDefinitionException;
import com.casumo.videorental.rental.exceptions.PricingHostNotFoundException;
import com.casumo.videorental.rental.model.Film;
import com.casumo.videorental.rental.model.FilmItem;
import com.casumo.videorental.rental.model.Order;
import com.casumo.videorental.rental.model.OrderItem;
import com.casumo.videorental.rental.repository.FilmItemRepository;
import com.casumo.videorental.rental.repository.FilmRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Slf4j
public class PricingClient {
    private static final String CANNOT_FIND_PRICE_RULE_MESSAGE = "Cannot find price rule definition for film item ";
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ISO_DATE_TIME.withZone(ZoneOffset.UTC);
    private static final String PROTOCOL = "http";
    private static final String PRICING_URL_SCHEMA = "%s://%s:%s/v1/prices";
    private static final String PRICING_BULK_URL_SCHEMA = "%s://%s:%s/v1/prices/bulk";

    private final RestTemplate restTemplate;
    private final FilmItemRepository filmItemRepository;
    private final FilmRepository filmRepository;
    private final String pricingServiceUrl;
    private final String pricingServiceBulkUrl;
    private final String pricingHost;
    private final String pricingPort;

    public PricingClient(RestTemplate restTemplate, Environment environment, FilmItemRepository filmItemRepository, FilmRepository filmRepository) {
        this.restTemplate = restTemplate;
        this.filmItemRepository = filmItemRepository;
        this.filmRepository = filmRepository;
        //TODO: this local http call is done to untangle renting module from pricing module when we make
        // separate microservices of them, then we can refactor this code and get pricing host from service registry (e.g. Eureka)
        String localServerPort = environment.getProperty("local.server.port");
        pricingPort = localServerPort != null ? localServerPort : environment.getProperty("server.port");
        try {
            pricingHost = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            throw new PricingHostNotFoundException("Pricing host cannot be resolved. Please check your configuration");
        }
        pricingServiceUrl = String.format(PRICING_URL_SCHEMA, PROTOCOL, pricingHost, pricingPort);
        pricingServiceBulkUrl = String.format(PRICING_BULK_URL_SCHEMA, PROTOCOL, pricingHost, pricingPort);
        log.info("Initialized pricing client with service URLs: {}, {}", pricingServiceUrl, pricingServiceBulkUrl);
    }

    /**
     * Get surcharges for film item.
     * Returns optional empty if amount cannot be calculated.
     *
     * @param filmItemId film item id
     * @return optional amount
     */
    public Optional<BigDecimal> getSurcharges(String filmItemId) {
        return filmItemRepository.findById(filmItemId)
                .flatMap(this::getSurcharges);
    }

    /**
     * Get total amount for ordered items as per their order duration.
     *
     * @param order an order
     * @return optional amount or empty if cannot calculate.
     */
    public Optional<BigDecimal> getTotalAmount(Order order) {
        try {
            PriceBulkRequest request = buildRequest(order);
            ResponseEntity<PriceBulkResponse> response = restTemplate.postForEntity(pricingServiceBulkUrl, request, PriceBulkResponse.class);
            if (response.getStatusCode() == HttpStatus.OK) {
                PriceBulkResponse priceBulkResponse = response.getBody();
                return Optional.of(priceBulkResponse.getTotal());
            }
        } catch (Exception exc) {
            log.error("Failed on getting price from remote server", exc);
        }
        return Optional.empty();
    }

    private PriceBulkRequest buildRequest(Order order) {
        return new PriceBulkRequest(order.getItems().stream()
                .map(this::buildRequestEntry).collect(Collectors.toList()));
    }

    private PriceBulkRequestEntry buildRequestEntry(OrderItem item) {
        String startTimeString = DATE_TIME_FORMATTER.format(item.getFrom());
        String endTimeString = DATE_TIME_FORMATTER.format(item.getTo());
        String priceRule = getPriceRule(item);
        return new PriceBulkRequestEntry(item.getFilmItemId(), priceRule, startTimeString, endTimeString);
    }

    private String getPriceRule(OrderItem item) {
        return filmItemRepository.findById(item.getFilmItemId())
                .map(FilmItem::getFilmId)
                .flatMap(filmRepository::findById)
                .map(Film::getPriceRuleId)
                .orElseThrow(() -> new NoPriceRuleDefinitionException(CANNOT_FIND_PRICE_RULE_MESSAGE + item.getFilmItemId()));
    }

    private String getPriceRule(FilmItem item) {
        return filmRepository.findById(item.getFilmId())
                .map(Film::getPriceRuleId)
                .orElseThrow(() -> new NoPriceRuleDefinitionException(CANNOT_FIND_PRICE_RULE_MESSAGE + item.getId()));
    }

    private Optional<BigDecimal> getSurcharges(FilmItem filmItem) {
        Assert.notNull(filmItem.getRentInfo(), "Cannot get surcharges for not borrowed item");

        String startTimeString = DATE_TIME_FORMATTER.format(filmItem.getRentInfo().getFrom());
        String endTimeString = DATE_TIME_FORMATTER.format(filmItem.getRentInfo().getTo());
        String nowTimeString = DATE_TIME_FORMATTER.format(Instant.now());
        String priceRule = getPriceRule(filmItem);

        Optional<BigDecimal> initialPrice = getAmount(startTimeString, endTimeString, priceRule);
        Optional<BigDecimal> currentPrice = getAmount(startTimeString, nowTimeString, priceRule);
        if (initialPrice.isPresent() && currentPrice.isPresent()) {
            BigDecimal initial = initialPrice.get();
            BigDecimal current = currentPrice.get();
            return initial.subtract(current).doubleValue() >= 0 ? Optional.of(BigDecimal.ZERO) : currentPrice;
        }
        return Optional.empty();
    }

    private PriceRequest buildRequest(String priceRule, String startTimeString, String endTimeString) {
        return new PriceRequest(priceRule, startTimeString, endTimeString);
    }

    private Optional<BigDecimal> getAmount(String startTimeString, String endTimeString, String priceRule) {
        PriceRequest request = buildRequest(priceRule, startTimeString, endTimeString);
        try {
            ResponseEntity<PriceResponse> response = restTemplate.postForEntity(pricingServiceUrl, request, PriceResponse.class);
            if (response.getStatusCode() == HttpStatus.OK) {
                PriceResponse priceResponse = response.getBody();
                return Optional.of(priceResponse.getPrice());
            }
        } catch (Exception exc) {
            log.error("Failed on getting price from remote server", exc);
        }
        return Optional.empty();
    }
}
