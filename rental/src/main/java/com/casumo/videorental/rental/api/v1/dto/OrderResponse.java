package com.casumo.videorental.rental.api.v1.dto;

import lombok.Value;

import java.time.Instant;
import java.util.List;

@Value
public class OrderResponse {
    private final String id;
    private final List<OrderResponseItem> items;
    private final String status;
    private Instant createdAt;
}
