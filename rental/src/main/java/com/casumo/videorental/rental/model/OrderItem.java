package com.casumo.videorental.rental.model;

import lombok.Value;

import java.time.Instant;

/**
 * An order item
 */
@Value
public class OrderItem {
    private final String filmItemId;
    private final String filmId;
    private final Instant from;
    private final Instant to;
}
