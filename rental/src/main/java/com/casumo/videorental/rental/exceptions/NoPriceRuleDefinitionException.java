package com.casumo.videorental.rental.exceptions;

public class NoPriceRuleDefinitionException extends RuntimeException {
    public NoPriceRuleDefinitionException(String message) {
        super(message);
    }
}
