package com.casumo.videorental.rental.repository;

import com.casumo.videorental.rental.model.Order;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OrderRepository extends MongoRepository<Order, String> {
}
