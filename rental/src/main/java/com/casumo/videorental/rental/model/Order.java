package com.casumo.videorental.rental.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.List;

@Document(collection = "rental_orders")
@AllArgsConstructor
@Builder(toBuilder = true)
@Getter
public class Order {
    private final String id;
    private final Instant createdAt;
    private final List<OrderItem> items;
    private final OrderState state;
    private final String borrowerId;
}
