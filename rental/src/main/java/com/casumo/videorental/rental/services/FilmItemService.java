package com.casumo.videorental.rental.services;

import com.casumo.videorental.rental.api.v1.dto.AddFilmItemRequest;
import com.casumo.videorental.rental.api.v1.dto.AddResponse;
import com.casumo.videorental.rental.api.v1.dto.FilmItemDto;
import com.casumo.videorental.rental.converters.FilmItemConverter;
import com.casumo.videorental.rental.exceptions.NoSuchFilmException;
import com.casumo.videorental.rental.model.Film;
import com.casumo.videorental.rental.model.FilmItem;
import com.casumo.videorental.rental.model.FilmItemState;
import com.casumo.videorental.rental.repository.FilmItemRepository;
import com.casumo.videorental.rental.repository.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class FilmItemService {

    private final FilmItemRepository filmItemRepository;
    private final FilmRepository filmRepository;
    private final FilmItemConverter filmItemConverter;


    @Autowired
    public FilmItemService(FilmItemRepository filmItemRepository, FilmRepository filmRepository, FilmItemConverter filmItemConverter) {
        this.filmItemRepository = filmItemRepository;
        this.filmRepository = filmRepository;
        this.filmItemConverter = filmItemConverter;
    }

    /**
     * Add new film item for a film
     *
     * @param addFilmItemRequest new film item add request
     * @return saved new film item
     */
    public AddResponse addFilmItem(AddFilmItemRequest addFilmItemRequest) {
        Film film = filmRepository.findById(addFilmItemRequest.getFilmId())
                .orElseThrow(() -> new NoSuchFilmException("Film does not exist, film id: " + addFilmItemRequest.getFilmId()));
        FilmItem filmItem = new FilmItem(addFilmItemRequest.getFilmItemId(), film.getId(), addFilmItemRequest.getDescription(),
                FilmItemState.ACTIVE, null);
        return new AddResponse(filmItemRepository.save(filmItem).getId());
    }

    /**
     * Find all film items with optional filmId filter
     *
     * @param filmIdFilter optional filmId filter
     * @return list of all film items
     */
    public List<FilmItemDto> findAll(String filmIdFilter) {
        Stream<FilmItem> resultStream = filmIdFilter != null ?
                filmItemRepository.findAllByFilmId(filmIdFilter) :
                filmItemRepository.findAll().stream();
        return resultStream
                .map(filmItemConverter::from)
                .collect(Collectors.toList());
    }

    /**
     * find film item by id
     *
     * @param filmItemId film item id
     * @return Optional film item or empty if not found
     */
    public Optional<FilmItemDto> findById(String filmItemId) {
        return filmItemRepository.findById(filmItemId)
                .map(filmItemConverter::from);
    }


    /**
     * Give back borrowed film item
     *
     * @param filmItemId film item id
     * @return Optional updated film item or empty if no item for return was found
     */
    public Optional<FilmItemDto> giveBack(String filmItemId) {
        return filmItemRepository.findByIdAndStateAndModify(filmItemId, FilmItemState.BORROWED, FilmItemState.ACTIVE, null)
                .map(filmItemConverter::from);
    }

}
