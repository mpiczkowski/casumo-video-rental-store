package com.casumo.videorental.rental.api.v1.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(onConstructor = @__(@JsonCreator))
@Getter
public class FilmItemDto {
    @JsonProperty(required = true)
    private final String filmId;

    @JsonProperty(required = true)
    private final String filmItemId;

    private final String description;

    private final RentInfoDto rentInfo;
}
