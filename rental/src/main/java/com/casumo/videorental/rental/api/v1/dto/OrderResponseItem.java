package com.casumo.videorental.rental.api.v1.dto;

import lombok.Value;

import java.time.Instant;

@Value
public class OrderResponseItem {
    private final String filmId;
    private final String filmItemId;
    private final Instant from;
    private final Instant to;
}
