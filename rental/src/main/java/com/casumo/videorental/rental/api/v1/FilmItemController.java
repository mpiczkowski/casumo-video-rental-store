package com.casumo.videorental.rental.api.v1;

import com.casumo.videorental.rental.api.v1.dto.AddFilmItemRequest;
import com.casumo.videorental.rental.api.v1.dto.FilmItemPriceDto;
import com.casumo.videorental.rental.services.FilmItemService;
import com.casumo.videorental.rental.services.PricingClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/v1")
public class FilmItemController {

    private final FilmItemService filmItemService;
    private final PricingClient pricingClient;

    public FilmItemController(FilmItemService filmItemService, PricingClient pricingClient) {
        this.filmItemService = filmItemService;
        this.pricingClient = pricingClient;
    }

    @RequestMapping(path = "/filmItems", method = RequestMethod.POST)
    public ResponseEntity addFilmItem(@RequestBody AddFilmItemRequest addFilmItemRequest) {
        //TODO: this could go into separate admin controller, ordinary users cannot add items
        return new ResponseEntity(filmItemService.addFilmItem(addFilmItemRequest), HttpStatus.OK);
    }

    @RequestMapping(path = "/filmItems", method = RequestMethod.GET)
    public ResponseEntity getFilmItems(@RequestParam(required = false, value = "filmId") String filmId) {
        return new ResponseEntity<>(filmItemService.findAll(filmId), HttpStatus.OK);
    }

    @RequestMapping(path = "/filmItems/{id}", method = RequestMethod.GET)
    public ResponseEntity getFilmItemById(@PathVariable("id") String id) {
        //TODO: after adding security need to protect it so that one user could not watch other user's rent details
        return filmItemService.findById(id)
                .map(i -> new ResponseEntity<>(i, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(path = "/filmItems/{id}/return", method = RequestMethod.POST)
    public ResponseEntity returnBorrowedItem(@PathVariable("id") String id) {
        //TODO: after adding security need to protect it so that one user could not give back other user's films
        return filmItemService.giveBack(id)
                .map(i -> new ResponseEntity<>(i, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(path = "/filmItems/{id}/surcharges", method = RequestMethod.GET)
    public ResponseEntity getPossibleSurcharges(@PathVariable("id") String id) {
        //TODO: after adding security need to protect it so that one user could not watch other user's surcharges
        return pricingClient.getSurcharges(id)
                .map(i -> new ResponseEntity<>(new FilmItemPriceDto(i.doubleValue()), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

}
