package com.casumo.videorental.rental.api.v1.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(onConstructor = @__(@JsonCreator))
@Getter
public class FilmItemPriceDto {
    private final Double amount;
}
