package com.casumo.videorental.rental.repository;

import com.casumo.videorental.rental.model.FilmItem;
import com.casumo.videorental.rental.model.FilmItemState;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.stream.Stream;

public interface FilmItemRepository extends MongoRepository<FilmItem, String>, FilmItemRepositoryEx {
    Stream<FilmItem> findAllByFilmId(String filmId);

    @Query("{ 'rentInfo.orderId' : ?0}")
    List<FilmItem> findAllByOrderId(String orderId);

    @Query("{ 'rentInfo.borrowerId' : ?0 , 'state' : ?1}")
    List<FilmItem> findAllByBorrowerIdAndState(String borrowerId, FilmItemState state);
}

