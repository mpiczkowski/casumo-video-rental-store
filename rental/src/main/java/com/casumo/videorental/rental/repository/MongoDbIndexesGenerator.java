package com.casumo.videorental.rental.repository;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.stereotype.Component;

@Component
public class MongoDbIndexesGenerator {

    private final MongoTemplate mongoTemplate;

    public MongoDbIndexesGenerator(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public void ensureIndexes() {
        ensureFilmItemDtoIndex();
        ensureFilmDtoIndex();
    }

    private void ensureFilmItemDtoIndex() {
        TextIndexDefinition textIndex = new TextIndexDefinition.TextIndexDefinitionBuilder()
                .onField("filmId")
                .build();

        mongoTemplate.indexOps("rental_film_items").ensureIndex(textIndex);
    }

    private void ensureFilmDtoIndex() {
        TextIndexDefinition textIndex = new TextIndexDefinition.TextIndexDefinitionBuilder()
                .onField("title")
                .onField("authors")
                .build();

        mongoTemplate.indexOps("rental_films").ensureIndex(textIndex);
    }

    //TODO: add other missing indexes
}
