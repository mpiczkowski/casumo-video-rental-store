package com.casumo.videorental.rental.repository;

import com.casumo.videorental.rental.model.FilmItem;
import com.casumo.videorental.rental.model.FilmItemState;
import com.casumo.videorental.rental.model.RentInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.Optional;

public class FilmItemRepositoryImpl implements FilmItemRepositoryEx {

    private final MongoTemplate mongoTemplate;

    @Autowired
    public FilmItemRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    /**
     * Update existing film item with new state and RentInfo.
     * The item is looked up by id and state.
     *
     * @param filmItemId      film item id
     * @param currentState    current film item state
     * @param updatedState    new film item state
     * @param updatedRentInfo new film item RentInfo
     * @return Optional updated item or empty if no item for update was found
     */

    @Override
    public Optional<FilmItem> findByIdAndStateAndModify(String filmItemId, FilmItemState currentState, FilmItemState updatedState, RentInfo updatedRentInfo) {
        Query query = Query.query(Criteria.where("id").is(filmItemId)
                .and("state").in(currentState));

        Update update = new Update()
                .set("state", updatedState)
                .set("rentInfo", updatedRentInfo);


        FindAndModifyOptions findAndModifyOptions = new FindAndModifyOptions()
                .upsert(false)
                .returnNew(true);

        return Optional.ofNullable(mongoTemplate.findAndModify(query, update, findAndModifyOptions, FilmItem.class));
    }

    /**
     * Update existing film item with new state and RentInfo.
     * The item is looked up by film id and state.
     *
     * @param filmIId      film id
     * @param currentState    current film item state
     * @param updatedState    new film item state
     * @param updatedRentInfo new film item RentInfo
     * @return Optional updated item or empty if no item for update was found
     */

    @Override
    public Optional<FilmItem> findByFilmIdAndStateAndModify(String filmIId, FilmItemState currentState, FilmItemState updatedState, RentInfo updatedRentInfo) {
        Query query = Query.query(Criteria.where("filmId").is(filmIId)
                .and("state").in(currentState));

        Update update = new Update()
                .set("state", updatedState)
                .set("rentInfo", updatedRentInfo);


        FindAndModifyOptions findAndModifyOptions = new FindAndModifyOptions()
                .upsert(false)
                .returnNew(true);

        return Optional.ofNullable(mongoTemplate.findAndModify(query, update, findAndModifyOptions, FilmItem.class));
    }
}
