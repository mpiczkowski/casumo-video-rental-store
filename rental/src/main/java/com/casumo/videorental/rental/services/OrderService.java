package com.casumo.videorental.rental.services;

import com.casumo.videorental.rental.api.v1.dto.AddOrderRequest;
import com.casumo.videorental.rental.api.v1.dto.AddOrderRequestItem;
import com.casumo.videorental.rental.api.v1.dto.OrderResponse;
import com.casumo.videorental.rental.api.v1.dto.OrderResponseItem;
import com.casumo.videorental.rental.events.FilmBorrowedEvent;
import com.casumo.videorental.rental.events.OrderPendingEvent;
import com.casumo.videorental.rental.events.OrderProcessedEvent;
import com.casumo.videorental.rental.exceptions.FilmUnavailableException;
import com.casumo.videorental.rental.model.*;
import com.casumo.videorental.rental.repository.FilmItemRepository;
import com.casumo.videorental.rental.repository.FilmRepository;
import com.casumo.videorental.rental.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class OrderService {
    private final OrderRepository orderRepository;
    private final FilmItemRepository filmItemRepository;
    private final FilmRepository filmRepository;
    private final ApplicationEventPublisher eventPublisher;
    private final PricingClient pricingClient;

    public OrderService(OrderRepository orderRepository, FilmItemRepository filmItemRepository, FilmRepository filmRepository, ApplicationEventPublisher eventPublisher, PricingClient pricingClient) {
        this.orderRepository = orderRepository;
        this.filmItemRepository = filmItemRepository;
        this.filmRepository = filmRepository;
        this.eventPublisher = eventPublisher;
        this.pricingClient = pricingClient;
    }


    /**
     * List all orders
     *
     * @return list of orders
     */
    public List<OrderResponse> findAll() {
        return orderRepository.findAll().stream()
                .map(o -> getOrderResponse(o))
                .collect(Collectors.toList());
    }

    /**
     * Get order by id
     *
     * @param orderId order id
     * @return optional order or empty if not found
     */
    public Optional<OrderResponse> get(String orderId) {
        return orderRepository.findById(orderId)
                .map(o -> getOrderResponse(o));
    }

    /**
     * Submit new order.
     * The order will be created in PENDING state.
     *
     * @param orderRequest order request
     * @return new order
     */
    public OrderResponse createNewOrder(AddOrderRequest orderRequest) {
        Assert.notEmpty(orderRequest.getItems(), "Order cannot be empty");

        String userId = getAuthenticatedUserId();
        Instant now = Instant.now();
        String orderId = UUID.randomUUID().toString();

        boolean areAllBooked = orderRequest.getItems()
                .stream()
                .map(i -> bookFilmItem(orderId, i, userId, now))
                .reduce(true, (a, b) -> a && b);

        if (!areAllBooked) {
            orderRollback(orderId);
            throw new FilmUnavailableException("One of the films in order is no longer available.");
        }

        Order order = new Order(orderId, now, getOrderedItems(userId), OrderState.PENDING, userId);
        order = orderRepository.save(order);

        eventPublisher.publishEvent(new OrderPendingEvent(order.getId()));
        return getOrderResponse(order);
    }

    /**
     * Process PENDING order.
     *
     * @param orderId an order id
     */
    public void processOrder(String orderId) {
        orderRepository.findById(orderId)
                .ifPresent(this::processOrder);
    }

    private Order processOrder(Order order) {
        log.info("order {} processing", order.getId());
        validate(order);

        log.info("order {} preparing for payment", order.getId());
        order = prepareForPay(order);

        log.info("order {} fetching amount to pay", order.getId());
        Optional<BigDecimal> totalPrice = pricingClient.getTotalAmount(order);
        if (!totalPrice.isPresent()) {
            log.info("order {} failed", order.getId());
            return fail(order);
        }

        log.info("order {} paying", order.getId());
        order = pay(order);

        log.info("order {} processed", order.getId());
        eventPublisher.publishEvent(new OrderProcessedEvent(order.getId()));
        return order;
    }

    private Order fail(Order order) {
        return orderRepository.save(order.toBuilder().state(OrderState.ERROR).build());
    }

    private Order pay(Order order) {
        //TODO: charge customer
        // if everything was fine then update items to BORROWED and order to PAID
        List<FilmItem> bookedItems = filmItemRepository.findAllByOrderId(order.getId())
                .stream()
                .map(i -> i.toBuilder().state(FilmItemState.BORROWED).build())
                .map(this::notifyAboutBorrowedFilm)
                .collect(Collectors.toList());
        filmItemRepository.saveAll(bookedItems);

        return orderRepository.save(order.toBuilder().state(OrderState.PAID).build());
    }

    private FilmItem notifyAboutBorrowedFilm(FilmItem filmItem) {
        filmRepository.findById(filmItem.getFilmId())
                .ifPresent(f -> eventPublisher.publishEvent(
                        new FilmBorrowedEvent(f.getPriceRuleId(), f.getId(), filmItem.getId(), filmItem.getRentInfo().getBorrowerId())));
        return filmItem;
    }

    private Order prepareForPay(Order order) {
        return orderRepository.save(order.toBuilder().state(OrderState.PAYMENT_PENDING).build());
    }

    private void validate(Order order) {
        if (order.getState() != OrderState.PENDING) {
            throw new IllegalStateException("Cannot process order which is not PENDING");
        }
    }

    private List<OrderItem> getOrderedItems(String userId) {
        return filmItemRepository.findAllByBorrowerIdAndState(userId, FilmItemState.BOOKED)
                .stream()
                .map(filmItem -> new OrderItem(filmItem.getId(), filmItem.getFilmId(), filmItem.getRentInfo().getFrom(), filmItem.getRentInfo().getTo()))
                .collect(Collectors.toList());
    }

    private OrderResponse getOrderResponse(Order o) {
        return new OrderResponse(o.getId(), getOrderResponseItems(o), o.getState().name(), o.getCreatedAt());
    }

    private List<OrderResponseItem> getOrderResponseItems(Order order) {
        return order.getItems().stream()
                .map(i -> new OrderResponseItem(i.getFilmId(), i.getFilmItemId(), i.getFrom(), i.getTo()))
                .collect(Collectors.toList());
    }

    //TODO: no security added yet, so dummy user Id used
    private String getAuthenticatedUserId() {
        return "123";
    }

    private boolean bookFilmItem(String orderId, AddOrderRequestItem item, String borrowerId, Instant borrowedFrom) {
        return filmItemRepository.findByFilmIdAndStateAndModify(item.getFilmId(), FilmItemState.ACTIVE, FilmItemState.BOOKED,
                new RentInfo(borrowerId, borrowedFrom, borrowedFrom.plus(item.getNumOfDays(), ChronoUnit.DAYS), orderId))
                .isPresent();
    }

    private List<FilmItem> orderRollback(String orderId) {
        return filmItemRepository.findAllByOrderId(orderId).stream()
                .map(i -> i.toBuilder().state(FilmItemState.ACTIVE).rentInfo(null).build())
                .map(filmItemRepository::save)
                .collect(Collectors.toList());
    }


}
