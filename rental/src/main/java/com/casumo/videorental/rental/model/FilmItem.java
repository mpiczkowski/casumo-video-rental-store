package com.casumo.videorental.rental.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "rental_film_items")
@AllArgsConstructor
@Builder(toBuilder = true)
@Getter
public class FilmItem {
    @Id
    private String id;

    private String filmId;
    private String description;
    private FilmItemState state;
    private RentInfo rentInfo;

}
