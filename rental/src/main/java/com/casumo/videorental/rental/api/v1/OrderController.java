package com.casumo.videorental.rental.api.v1;

import com.casumo.videorental.rental.api.v1.dto.AddOrderRequest;
import com.casumo.videorental.rental.services.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/v1")
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @RequestMapping(path = "/orders", method = RequestMethod.POST)
    public ResponseEntity addOrder(@RequestBody @Valid AddOrderRequest addOrderRequest) {
        return new ResponseEntity(orderService.createNewOrder(addOrderRequest), HttpStatus.OK);
    }

    @RequestMapping(path = "/orders/{uuid}", method = RequestMethod.GET)
    public ResponseEntity getOrderById(@PathVariable("uuid") String uuid) {
        return orderService.get(uuid)
                .map(i -> new ResponseEntity<>(i, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(path = "/orders", method = RequestMethod.GET)
    public ResponseEntity getAllOrders() {
        return new ResponseEntity(orderService.findAll(), HttpStatus.OK);
    }
}
