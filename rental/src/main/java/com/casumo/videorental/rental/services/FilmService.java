package com.casumo.videorental.rental.services;

import com.casumo.videorental.rental.api.v1.dto.AddFilmRequest;
import com.casumo.videorental.rental.api.v1.dto.AddResponse;
import com.casumo.videorental.rental.api.v1.dto.FilmDto;
import com.casumo.videorental.rental.converters.FilmConverter;
import com.casumo.videorental.rental.exceptions.FilmAlreadyExistsException;
import com.casumo.videorental.rental.model.Film;
import com.casumo.videorental.rental.repository.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class FilmService {

    private final FilmRepository filmRepository;
    private final FilmConverter filmConverter;

    @Autowired
    public FilmService(FilmRepository filmRepository, FilmConverter filmConverter) {
        this.filmRepository = filmRepository;
        this.filmConverter = filmConverter;
    }

    /**
     * Add new film
     *
     * @param addFilmRequest a request to add new film
     * @return added film response
     */
    public AddResponse add(AddFilmRequest addFilmRequest) {
        if (filmRepository.findByTitle(addFilmRequest.getTitle()).isPresent()) {
            throw new FilmAlreadyExistsException("Film already exists: " + addFilmRequest.getTitle());
        }
        Film saved = filmRepository.save(new Film(UUID.randomUUID().toString(), addFilmRequest.getTitle(), addFilmRequest.getDirectors(), addFilmRequest.getPriceRuleName()));
        return new AddResponse(filmConverter.from(saved).getFilmId());
    }

    /**
     * Find all films paged
     *
     * @param pageable page definition
     * @return paged films
     */
    public Page<FilmDto> findAll(Pageable pageable) {
        return filmRepository.findAll(pageable)
                .map(f -> new FilmDto(f.getId(), f.getTitle(), f.getDirectors(), f.getPriceRuleId()));
    }

    /**
     * Find film item by id
     *
     * @param filmItemId film item id
     * @return Optional film item or empty if not found.
     */
    public Optional<FilmDto> findById(String filmItemId) {
        return filmRepository.findById(filmItemId)
                .map(filmConverter::from);
    }

}

