package com.casumo.videorental.rental.converters;

import com.casumo.videorental.rental.api.v1.dto.FilmDto;
import com.casumo.videorental.rental.model.Film;
import org.springframework.stereotype.Component;

@Component
public class FilmConverter {

    public FilmDto from(Film film) {
        return new FilmDto(film.getId(), film.getTitle(), film.getDirectors(), film.getPriceRuleId());
    }

}
