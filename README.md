# Video Rental Store
This is a solution to the [problem](docs/problem.md).
The description of the solution with explanation of decisions taken and methodologies can be found [here](docs/solution.md)

## Prerequisites: 
Maven and JDK 8 installed.

## Building

You can build the application with the following command executed in root folder of the project

```
mvn clean install 

```
or with `docker` profile (if docker is installed)

```
mvn clean install -Pdocker
```

It will run unit and integration tests.

## Running

You can run the application with the following command executed in root folder of the project

```
mvn spring-boot:run -f app/pom.xml
```

or

```
java -jar app/target/app-0.0.1-SNAPSHOT.jar 
```

or inside docker container (if built with `docker` profile)

NOTE: it requires docker-compose installed.

```
docker-compose up

```

When application will start it will load all the default price rules: for new releases, regular and old films.

## Usage

Postman collection is [here](docs/video-rental-store.postman_collection.json)

Below is an example usage:

### Pricing

#### Calculate prices for multiple items
```
curl -X POST -H 'Content-Type:application/json' -d '{"items": [{"uuid": "123", "rule":"regular", "start": "2011-12-03T10:15:30+01:00","end" : "2011-12-03T12:15:30+01:00" }]}' http://localhost:8080/v1/prices/bulk
```
e.g. response: 

```
{"items":[{"uuid":"123","price":30}],"total":30}
```

#### Calculate prices for single item
```
curl -X POST -H 'Content-Type:application/json' -d '{"rule":"regular", "start": "2011-12-03T10:15:30+01:00","end" : "2011-12-03T12:15:30+01:00"}' http://localhost:8080/v1/prices
```

e.g. response: 

```
{"price":30}
```


### Rentals


#### Add new film

```
curl -X POST -H 'Content-Type:application/json' -d '{"directors": ["Alfred Hitchcock"], "title" : "The Birds", "ruleName" : "regular"}' http://localhost:8080/v1/films
curl -X POST -H 'Content-Type:application/json' -d '{"directors": ["Steven Spielberg"], "title" : "Jurasick Park I", "ruleName" : "regular"}' http://localhost:8080/v1/films
```

e.g. response: 

```
{"id":"9638dc8f-8b08-49b5-b707-9d222567b973"}
```

#### Add physical items for the film (it will update the film item if the filmItemId already exists)

```
curl -X POST -H 'Content-Type:application/json' -d '{"filmId" : "9638dc8f-8b08-49b5-b707-9d222567b973", "filmItemId": "M12345"}' http://localhost:8080/v1/filmItems


curl -X POST -H 'Content-Type:application/json' -d '{"filmId" : "9638dc8f-8b08-49b5-b707-9d222567b973", "filmItemId": "L09732", "description" : "Red cover"}' http://localhost:8080/v1/filmItems
```

e.g. response: 

```
{"id":"cc1f4c79-a51b-4647-9abb-a7bbe6474bb1"}
```


#### List film items

```
 curl -H 'Content-Type:application/json' http://localhost:8080/v1/filmItems
 curl -H 'Content-Type:application/json' http://localhost:8080/v1/filmItems?filmId=9638dc8f-8b08-49b5-b707-9d222567b973
```

e.g. response: 

```
[
    {
        "filmId": "9638dc8f-8b08-49b5-b707-9d222567b973",
        "filmItemId": "T2",
        "description": "got from Jack",
        "rentInfo": {
            "from": "2018-04-24T17:41:24.633Z",
            "to": "2018-04-26T17:41:24.633Z"
        }
    }
    {
        "filmId": "9638dc8f-8b08-49b5-b707-9d222567b973",
        "filmItemId": "M2",
        "description": "vantage cover",
        "rentInfo": null
    }
]
```

#### List films

```
url -H 'Content-Type:application/json' localhost:8080/v1/films?page=0&size=100
```

e.g. response: 

```
{
    "content": [
        {
            "filmId": "1ad37396-030e-4ea8-811e-21860f0a9a2f",
            "title": "Matrix",
            "directors": [
                "Lana Wachowski",
                "Lilly Wachowski"
            ],
            "priceRuleName": "standard"
        },
        {
            "filmId": "15a1875e-fe42-43ad-9c2a-b46be0bc83a0",
            "title": "Tomb Reider",
            "directors": [
                "Roar Uthaug"
            ],
            "priceRuleName": "standard"
        }
    ],
    "pageable": {
        "sort": {
            "sorted": true,
            "unsorted": false
        },
        "pageSize": 100,
        "pageNumber": 0,
        "offset": 0,
        "paged": true,
        "unpaged": false
    },
    "totalPages": 1,
    "totalElements": 2,
    "last": true,
    "first": true,
    "numberOfElements": 2,
    "sort": {
        "sorted": true,
        "unsorted": false
    },
    "size": 100,
    "number": 0
}

```

# TODOs

1. `/v1/films` response is paged while other listing endpoints are not. We need some consistency here.
2. Missing scheduled job which would check dangling BOOKED and BORROWED film items without corresponding Order
3. Implement tests which have only placeholders
4. Missing endpoints to update and delete Films
5. In `/v1/prices` endpoints need to validate if start date is not after end date
6. Add security and protect that one user cannot see other user's borrowed items, surcharges, orders and cannot return other user's films.
7. Payments not implemented
8. REST API docs would be useful, e.g. using [Swagger2](http://www.baeldung.com/swagger-2-documentation-for-spring-rest-api)
9. Improve build - separate unit tests form integration tests in different Maven profiles, add test coverage report (Jacoco)
.. surely still more to do :)